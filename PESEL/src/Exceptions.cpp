#include <librr/PESEL/Exceptions.hpp>

#include <string>

namespace librr
{
namespace pesel
{

NullPointerProvided::NullPointerProvided()
    : std::invalid_argument("Null pointer provided")
{
}

InvalidLength::InvalidLength(const char *peselCandidate, const std::size_t length)
    : std::length_error{std::string{peselCandidate} + ": Invalid PESEL length: " + std::to_string(length)}
{
}

InvalidCharacter::InvalidCharacter(const char *peselCandidate, char invalidChar)
    : std::domain_error{std::string{peselCandidate} + ": Invalid character : '" + invalidChar + "'"}
{
}

ChecksumMismatch::ChecksumMismatch(const char *peselCandidate, unsigned expectedChecksum, unsigned actualChecksum)
    : std::runtime_error{
        std::string{peselCandidate} + ": Invalid checksum : "
            + std::to_string(actualChecksum) + ", should be: " + std::to_string(expectedChecksum)}
{
}

InvalidDate::InvalidDate(const char *peselCandidate, uint16_t year, uint8_t month, uint8_t day)
    : std::domain_error{std::string{peselCandidate} + ": Invalid date: "
        + std::to_string(day) + "." + std::to_string(month) + "." + std::to_string(year)}
{
}

}  // namespace pesel
}  // namespace librr