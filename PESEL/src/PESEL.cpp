#include <librr/PESEL/PESEL.hpp>

#include <cassert>
#include <cstring>

#include <librr/PESEL/Exceptions.hpp>

namespace librr
{
namespace pesel
{

namespace
{

uint8_t calculateChecksum(uint8_t digits[])
{
    return (9 * digits[0]
        + 7 * digits[1]
        + 3 * digits[2]
        + 1 * digits[3]
        + 9 * digits[4]
        + 7 * digits[5]
        + 3 * digits[6]
        + 1 * digits[7]
        + 9 * digits[8]
        + 7 * digits[9]) % 10;
}

uint16_t fetchYear(uint8_t digits[])
{
    uint8_t yearShort = digits[0]*10 + digits[1];
    switch ((digits[2]*10 + digits[3])/20)
    {
        case 0:
            return 1900 + yearShort;
        case 1:
            return 2000 + yearShort;
        case 2:
            return 2100 + yearShort;
        case 3:
            return 2200 + yearShort;
        case 4:
            return 1800 + yearShort;
    }
    throw std::runtime_error{"Should not be here"};
}

uint16_t fetchMonth(uint8_t digits[])
{
    return (digits[2]*10 + digits[3])%20;
}

uint16_t fetchDay(uint8_t digits[])
{
    return digits[4]*10 + digits[5];
}

uint16_t fetchIfIsMale(uint8_t digits[])
{
    return 1 == digits[9] % 2;
}

bool isValidDate(uint16_t year, uint8_t month, uint8_t day)
{
    assert(year>=1800);
    assert(year<2300);
    if (month == 0 || month > 12)
    {
        return false;
    }
    if (day == 0 || day > 31)
    {
        return false;
    }
    switch (month)
    {
        case 4:
        case 6:
        case 9:
        case 11:
        {
            if (31 == day)
            {
                return false;
            }
            break;
        }
        case 2:
        {
            if (day > 29)
            {
                return false;
            }
            if (day < 29)
            {
                return true;
            }
            if (year%4 != 0)
            {
                return false;
            }
            if (year%400 == 0)
            {
                return true;
            }
            if (year%100 == 0)
            {
                return false;
            }
            break;
        }
    }
    return true;
}

}  // namespace

PESEL::PESEL(const char *peselCandidateStr)
{
    if (nullptr == peselCandidateStr)
    {
        throw NullPointerProvided{};
    }
    uint8_t digits[11];
    for (std::size_t i = 0; i < 11; ++i)
    {
        const char c = peselCandidateStr[i];
        if ('\0' == c)
        {
            throw InvalidLength{peselCandidateStr, i};
        }
        if (c < '0' || c > '9')
        {
            throw InvalidCharacter{peselCandidateStr, c};
        }
        digits[i] = c - '0';
    }
    if ('\0' != peselCandidateStr[11])
    {
        throw InvalidLength{peselCandidateStr, strlen(peselCandidateStr)};
    }

    uint8_t checksum = calculateChecksum(digits);
    if (digits[10] != checksum)
    {
        throw ChecksumMismatch{peselCandidateStr, digits[10], checksum};
    }

    birthYear_ = fetchYear(digits);
    birthMonth_ = fetchMonth(digits);
    birthDay_ = fetchDay(digits);
    if (!isValidDate(birthYear_, birthMonth_, birthDay_))
    {
        throw InvalidDate{peselCandidateStr, birthYear_, birthMonth_, birthDay_};
    }
    male_ = fetchIfIsMale(digits);
}

std::optional<PESEL> PESEL::createSafely(const char *peselCandidateStr)
{
    try
    {
        return PESEL{peselCandidateStr};
    }
    catch (const std::exception &)
    {
        return std::nullopt;
    }
}

bool PESEL::isCorrect(const char *peselCandidateStr)
{
    return std::nullopt != createSafely(peselCandidateStr);
}

uint16_t PESEL::birthYear() const
{
    return birthYear_;
}

uint8_t PESEL::birthMonth() const
{
    return birthMonth_;
}

uint8_t PESEL::birthDay() const
{
    return birthDay_;
}

bool PESEL::isMale() const
{
    return male_;
}

}  // namespace pesel
}  // namespace librr