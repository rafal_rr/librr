#include <optional>
#include <gtest/gtest.h>
#include <librr/PESEL/PESEL.hpp>
#include <librr/PESEL/Exceptions.hpp>

namespace librr
{
namespace pesel
{
namespace ut
{

const char *correctPesel = "44051401359";

void printPeselCtorException(const char *peselCandidate)
{
    try
    {
        PESEL p{peselCandidate};
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        throw;
    }
}

TEST(PESELShould, notBeCreatedIfThereAreTooLessDigits)
{
    const char peselCandidate[] = "4405140135";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), InvalidLength);
}

TEST(PESELShould, notBeCreatedForEmptyString)
{
    const char peselCandidate[] = "";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), InvalidLength);
}

TEST(PESELShould, notBeCreatedForNull)
{
    const char *peselCandidate = nullptr;
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), NullPointerProvided);
}

TEST(PESELShould, notBeCreatedIfThereAreTooManyDigits)
{
    const char peselCandidate[] = "440514013599";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), InvalidLength);
}

TEST(PESELShould, notBeCreatedIfThereAreNonDigitsInString)
{
    const char peselCandidate[] = "44051401f59";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), InvalidCharacter);
}

TEST(PESELShould, notBeCreatedIfThereIsAChecksumMismatch)
{
    const char peselCandidate[] = "44051401358";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), ChecksumMismatch);
}

TEST(PESELShould, fetchAllInformationsCorrectly)
{
    const char *peselCandidate = correctPesel;

    auto createdPesel = PESEL::createSafely(peselCandidate);
    ASSERT_NE(std::nullopt, createdPesel);

    EXPECT_EQ(1944, createdPesel->birthYear());
    EXPECT_EQ(5, createdPesel->birthMonth());
    EXPECT_EQ(14, createdPesel->birthDay());
    EXPECT_TRUE(createdPesel->isMale());
}

TEST(PESELShould, notAllowToPassIncorrectDate)
{
    const char peselCandidate[] = "44023001356";
    EXPECT_EQ(std::nullopt, PESEL::createSafely(peselCandidate));
    EXPECT_THROW(printPeselCtorException(peselCandidate), InvalidDate);
}

// FIXME: TEST_P

TEST(PESELShould, notAllowToPassIncorrectDate_regularNonLeapYears)
{
    ASSERT_NE(std::nullopt, PESEL::createSafely("47022812341"));

    EXPECT_EQ(std::nullopt, PESEL::createSafely("47022912348"));
    EXPECT_THROW(printPeselCtorException("47022912348"), InvalidDate);

    EXPECT_EQ(std::nullopt, PESEL::createSafely("47023012344"));
    EXPECT_THROW(printPeselCtorException("47023012344"), InvalidDate);
}

TEST(PESELShould, notAllowToPassIncorrectDate_regularLeapYears)
{
    ASSERT_NE(std::nullopt, PESEL::createSafely("48022812348"));
    ASSERT_NE(std::nullopt, PESEL::createSafely("48022912345"));

    EXPECT_EQ(std::nullopt, PESEL::createSafely("48023012341"));
    EXPECT_THROW(printPeselCtorException("48023012341"), InvalidDate);
}

TEST(PESELShould, notAllowToPassIncorrectDate_nonLeapYearsDividibleBy100)
{
    ASSERT_NE(std::nullopt, PESEL::createSafely("00022812346"));

    EXPECT_EQ(std::nullopt, PESEL::createSafely("00022912343"));
    EXPECT_THROW(printPeselCtorException("00022912343"), InvalidDate);

    EXPECT_EQ(std::nullopt, PESEL::createSafely("00023012349"));
    EXPECT_THROW(printPeselCtorException("00023012349"), InvalidDate);
}

TEST(PESELShould, notAllowToPassIncorrectDate_leapYearsDividibleBy400)
{
    ASSERT_NE(std::nullopt, PESEL::createSafely("00222812342"));
    ASSERT_NE(std::nullopt, PESEL::createSafely("00222912349"));

    EXPECT_EQ(std::nullopt, PESEL::createSafely("00223012345"));
    EXPECT_THROW(printPeselCtorException("00223012345"), InvalidDate);
}

TEST(PESELShould, allowCreateNumbersForDatesBetween1800and2300)
{
    PESEL p1900{"00110112341"};
    EXPECT_EQ(1900, p1900.birthYear());
    EXPECT_EQ(11, p1900.birthMonth());

    PESEL p2000{"00310112347"};
    EXPECT_EQ(2000, p2000.birthYear());
    EXPECT_EQ(11, p2000.birthMonth());

    PESEL p2100{"00510112343"};
    EXPECT_EQ(2100, p2100.birthYear());
    EXPECT_EQ(11, p2100.birthMonth());

    PESEL p2200{"00710112349"};
    EXPECT_EQ(2200, p2200.birthYear());
    EXPECT_EQ(11, p2200.birthMonth());

    PESEL p1800{"00910112345"};
    EXPECT_EQ(1800, p1800.birthYear());
    EXPECT_EQ(11, p1800.birthMonth());
}

}  // namespace ut
}  // namespace pesel
}  // namespace librr