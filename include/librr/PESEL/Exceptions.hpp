#ifndef __LIBRR__PESEL__EXCEPTIONS_HPP__
#define __LIBRR__PESEL__EXCEPTIONS_HPP__

#include <stdexcept>

namespace librr
{
namespace pesel
{

class InvalidLength : public std::length_error
{
public:
    explicit InvalidLength(const char *peselCandidate, const std::size_t length);
};

class NullPointerProvided : public std::invalid_argument
{
public:
    explicit NullPointerProvided();
};

class InvalidCharacter : public std::domain_error
{
public:
    explicit InvalidCharacter(const char *peselCandidate, char invalidChar);
};

class ChecksumMismatch : public std::runtime_error
{
public:
    explicit ChecksumMismatch(const char *peselCandidate, unsigned expectedChecksum, unsigned actualChecksum);
};

class InvalidDate : public std::domain_error
{
public:
    explicit InvalidDate(const char *peselCandidate, uint16_t year, uint8_t month, uint8_t day);
};

}  // namespace pesel
}  // namespace librr

#endif  // __LIBRR__PESEL__EXCEPTIONS_HPP__