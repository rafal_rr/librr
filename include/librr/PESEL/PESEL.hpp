#ifndef __LIBRR__PESEL__PESEL_HPP__
#define __LIBRR__PESEL__PESEL_HPP__

#include <cstdint>
#include <optional>

namespace librr
{
namespace pesel
{

class PESEL
{
public:
    explicit PESEL(const char *peselCandidateStr);
    static bool isCorrect(const char *peselCandidateStr);
    static std::optional<PESEL> createSafely(const char *peselCandidateStr);

    uint16_t birthYear() const;
    uint8_t birthMonth() const;
    uint8_t birthDay() const;
    bool isMale() const;
private:
    uint16_t birthYear_;
    uint8_t birthMonth_;
    uint8_t birthDay_;
    bool male_;
};

}  // namespace pesel
}  // namespace librr

#endif  // __LIBRR__PESEL__PESEL_HPP__