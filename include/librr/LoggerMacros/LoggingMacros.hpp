#ifndef __LIBRR__LOGGER_MACROS__LOGGING_MACROS_HPP__
#define __LIBRR__LOGGER_MACROS__LOGGING_MACROS_HPP__

#include <ios>
#include <sstream>
#include "TraceFunctions.hpp"
#include "TraceRecord.hpp"

template <typename T>
std::string repeatNTimes(const T &text, int N)
{
    std::ostringstream oss;
    for (int i = 0; i < N; ++i)
    {
        oss << text;
    }
    return oss.str();
}

#ifndef LM_EOL
#define LM_EOL std::endl
#endif  // LM_EOL

#define LOG(Text) LM_LOGGING_STREAM << Text << LM_EOL

#define LOG_N(Text, N) LM_LOGGING_STREAM << repeatNTimes(Text, N) << LM_EOL

#define LOG_VAL(Var) LM_LOGGING_STREAM << (#Var) << " = " << (Var) << LM_EOL

#define LOG_VAL_CAST(Var, DestType) LM_LOGGING_STREAM << (#Var) << " = " << static_cast<DestType>(Var) << LM_EOL

#define LOG_VAL_AS(Var, Name) LM_LOGGING_STREAM << (Name) << " = " << (Var) << LM_EOL

#define LOG_BOOL(Var) LM_LOGGING_STREAM << std::boolalpha << (#Var) << " = " << (Var) << LM_EOL

#define LOG_VAL_I(Arr, Index) LM_LOGGING_STREAM << (#Arr) << "[" << Index << "] = " << Arr[Index] << LM_EOL

#define TRACE() \
    LM_LOGGING_STREAM \
    << librr::loggingmacros::traceMacroFunction( \
        librr::loggingmacros::makeTraceRecord( \
            __LINE__, \
            __func__, \
            __PRETTY_FUNCTION__, \
            __FILE__)) \
    << LM_EOL

#endif  // __LIBRR__LOGGER_MACROS__LOGGING_MACROS_HPP__