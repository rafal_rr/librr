#ifndef __LIBRR__LOGGER_MACROS__TRACE_FUNCTIONS_HPP__
#define __LIBRR__LOGGER_MACROS__TRACE_FUNCTIONS_HPP__

#include <string>

namespace librr
{
namespace loggingmacros
{

class TraceRecord;

std::string defaultTraceFunction(const TraceRecord &record);

typedef std::string (*TraceFunction)(const TraceRecord &);

extern TraceFunction traceMacroFunction;

}  // namespace loggingmacros
}  // namespace librr

#endif  // __LIBRR__LOGGER_MACROS__TRACE_FUNCTIONS_HPP__