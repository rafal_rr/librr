#ifndef __LIBRR__LOGGER_MACROS__STD_OUT_LOGGING_MACROS_HPP__
#define __LIBRR__LOGGER_MACROS__STD_OUT_LOGGING_MACROS_HPP__

#include <iosfwd>

#define LM_LOGGING_STREAM std::cout
#include "LoggingMacros.hpp"

#endif  // __LIBRR__LOGGER_MACROS__STD_OUT_LOGGING_MACROS_HPP__