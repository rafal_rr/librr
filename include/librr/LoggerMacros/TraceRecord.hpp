#ifndef __LIBRR__LOGGER_MACROS__TRACE_RECORD_HPP__
#define __LIBRR__LOGGER_MACROS__TRACE_RECORD_HPP__

#include <string>

namespace librr
{
namespace loggingmacros
{

struct TraceRecord
{
    const int line;
    const char *functionName;
    const char *prettyFunctionName;
    const char *fileName;
};

TraceRecord makeTraceRecord(
    const int line,
    const char *functionName,
    const char *prettyFunctionName,
    const char *fileName);

}  // namespace loggingmacros
}  // namespace librr

#endif  // __LIBRR__LOGGER_MACROS__TRACE_RECORD_HPP__