#include <sstream>
#include <gtest/gtest.h>

#define LM_LOGGING_STREAM oss
#include <librr/LoggerMacros/LoggingMacros.hpp>

class LoggerMacroFixture : public ::testing::Test
{
protected:
    std::ostringstream oss;
};

TEST_F(LoggerMacroFixture, LOG_macroSimpleTest)
{
    LOG("abc");
    EXPECT_EQ("abc\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_N_test)
{
    LOG_N("abc", 5);
    EXPECT_EQ("abcabcabcabcabc\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_VAL_test)
{
    int i =101;
    LOG_VAL(i);
    EXPECT_EQ("i = 101\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_VAL_AS_test)
{
    int i =101;
    LOG_VAL_AS(i, "Iterator");
    EXPECT_EQ("Iterator = 101\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_VAL_I_testForArray)
{
    double dArr[5] = {5.6, 0.0, -1.6, 2.2, 4.4};
    LOG_VAL_I(dArr, 2);
    EXPECT_EQ("dArr[2] = -1.6\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_VAL_I_testForVector)
{
    std::vector<unsigned> uVec;
    uVec.push_back(890);
    uVec.push_back(55);
    uVec.push_back(340);
    uVec.push_back(431);
    LOG_VAL_I(uVec, 3);
    EXPECT_EQ("uVec[3] = 431\n", oss.str());
}

TEST_F(LoggerMacroFixture, LOG_BOOL_test)
{
    bool b = true;
    LOG_BOOL(b);
    EXPECT_EQ("b = true\n", oss.str());
}

TEST_F(LoggerMacroFixture, TRACE_simpleTest)
{
    TRACE();int traceLine = __LINE__;
    std::ostringstream expectedSs;
    expectedSs << __PRETTY_FUNCTION__ << " in " << __FILE__ << ": " << traceLine << "\n";
    EXPECT_EQ(expectedSs.str(), oss.str());
}

std::string myTraceFunction(const librr::loggingmacros::TraceRecord &rec)
{
    std::ostringstream oss;
    oss << "my trace! " << rec.functionName << " in file " << rec.fileName << ", line: " << rec.line;
    return oss.str();
}

TEST_F(LoggerMacroFixture, TRACE_myFunction)
{
    librr::loggingmacros::traceMacroFunction = &myTraceFunction;
    TRACE();int traceLine = __LINE__;
    EXPECT_EQ(
        (myTraceFunction(
            librr::loggingmacros::makeTraceRecord(
                traceLine,
                __func__,
                __PRETTY_FUNCTION__,
                __FILE__)) + "\n"),
        oss.str());
}