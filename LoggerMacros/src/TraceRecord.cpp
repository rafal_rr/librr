#include <librr/LoggerMacros/TraceRecord.hpp>

namespace librr
{
namespace loggingmacros
{

TraceRecord makeTraceRecord(
    const int line,
    const char *functionName,
    const char *prettyFunctionName,
    const char *fileName)
{
    TraceRecord result = {line, functionName, prettyFunctionName, fileName};
    return result;
}

}  // namespace loggingmacros
}  // namespace librr