#include <sstream>
#include <librr/LoggerMacros/TraceFunctions.hpp>
#include <librr/LoggerMacros/TraceRecord.hpp>

namespace librr
{
namespace loggingmacros
{

std::string defaultTraceFunction(const TraceRecord &record)
{
    std::ostringstream oss;
    oss << record.prettyFunctionName << " in " << record.fileName << ": " << record.line;
    return oss.str();
}

TraceFunction traceMacroFunction = &defaultTraceFunction;

}  // namespace loggingmacros
}  // namespace librr